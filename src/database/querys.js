export const querys = {
    getAllProducts: "SELECT TOP(500) * FROM [webstore].[dbo].[Productos]",
    getProducById: "SELECT * FROM Productos Where Id = @Id",
    addNewProduct:
      "INSERT INTO [webstore].[dbo].[Productos] (name, description, quantity) VALUES (@name,@description,@quantity);",
    deleteProduct: "DELETE FROM [webstore].[dbo].[Productos] WHERE Id= @Id",
    getTotalProducts: "SELECT COUNT(*) FROM webstore.dbo.Productos",
    updateProductById:
      "UPDATE [webstore].[dbo].[Productos] SET Name = @name, Description = @description, Quantity = @quantity WHERE Id = @id",
  };
  